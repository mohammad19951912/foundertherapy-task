from dateutil.relativedelta import relativedelta
from django.db.models import Q, Avg, Count
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from .models import Check, Employee
from rest_framework.response import Response
from .serializers import CheckSerializer, VactionSerializer
from .utiltes import django_dataframe


class CheckInAPIView(CreateAPIView):
    serializer_class = CheckSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.check_valide_action()
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response('checked in', status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(employee=self.request.user.employee)

    def check_valide_action(self):
        try:
            latest_check_in = self.request.user.employee.check_set.latest('check_in')
            if latest_check_in.check_out is None:
                raise ValidationError("this user must check out before check in")
        except Check.DoesNotExist:
            pass


class CheckOutAPIView(APIView):
    def post(self, request):
        latest_check_in = self.get_last_open_check_in()
        latest_check_in.check_out = timezone.now()
        latest_check_in.save()
        return Response('checked out')

    def get_last_open_check_in(self) -> Check:
        try:
            latest_check_in = self.request.user.employee.check_set.latest('check_in')
            if latest_check_in.check_out is not None:
                raise ValidationError("this user must check in before check out")
            return latest_check_in
        except Check.DoesNotExist:
            raise NotFound("please check in before")


class AddVacationAPIView(CreateAPIView):
    serializer_class = VactionSerializer

    def perform_create(self, serializer):
        serializer.save(employee=self.request.user.employee)


class GetTotalWorkingHoursAPIView(APIView):
    def get(self, request):
        total_type = request.GET.get('type', None)
        if total_type is None:
            raise ValidationError("please choice type")
        if total_type.lower() not in ('year', 'quarter', 'week'):
            raise ValidationError("please choice valid type")
        if total_type.lower() == 'year':
            response = {'values': self.year_total_hours(request)}
        if total_type.lower() == 'quarter':
            response = {'values': self.quarter_total_hours(request)}
        if total_type.lower() == 'week':
            response = {'values': self.week_total_hours()}
        return Response(response)

    def year_total_hours(self, request):
        year = request.GET.get('year', None)
        try:
            year = int(year)
        except ValueError:
            raise ValidationError("please enter valid value for year")
        check_df = self.get_query_set(year)
        return self.get_groupedby_houres(check_df)

    def quarter_total_hours(self, request):
        quarter = request.GET.get('quarter', None)
        year = request.GET.get('year', None)

        try:
            quarter = int(quarter)
            year = int(year)
        except ValueError:
            raise ValidationError("please enter valid value for year")

        check_df = self.get_quarter_query_set(year, quarter)
        return self.get_groupedby_houres(check_df)

    def week_total_hours(self):
        check_df = self.get_week_query_set()
        return self.get_groupedby_houres(check_df)

    def get_groupedby_houres(self, check_df):
        check_df['duration'] = check_df['check_out'] - check_df['check_in']
        work_houres_dick = check_df.groupby('employee')['duration'].agg('sum').to_dict()
        work_houres = {
            key: value.total_seconds() / 3600 for key, value in work_houres_dick.items()
        }
        return work_houres

    @django_dataframe
    def get_query_set(self, year):
        return Check.objects.filter(check_in__year=year)

    @django_dataframe
    def get_quarter_query_set(self, year, quarter):
        current_year = timezone.datetime(year=year, month=1, day=1)
        start_quarter = current_year + relativedelta(months=(quarter - 1) * 3)
        end_quarter = start_quarter + relativedelta(months=3)
        return Check.objects.filter(Q(check_in__gte=start_quarter) & Q(check_in__lte=end_quarter))

    @django_dataframe
    def get_week_query_set(self):
        today = timezone.now()
        previous_week = timezone.now() - relativedelta(days=7)
        return Check.objects.filter(Q(check_in__gte=today) & Q(check_in__lte=previous_week))


class GetAverageArrivalAndLeavingTime(APIView):
    def get(self, request):
        avg_hours_for_employee = []
        for employee in Employee.objects.all():
            checks_for_employee = employee.check_set
            check_ins = checks_for_employee.values_list('check_in', flat=True)
            check_outs = checks_for_employee.values_list('check_out', flat=True)
            check_in_hours = map(lambda time: time.hour + (time.minute / 60.0), check_ins)
            check_out_hours = map(lambda time: time.hour + (time.minute / 60.0), check_outs)
            check_in_avg_hours = sum(check_in_hours) / Check.objects.all().count()
            check_out_avg_hours = sum(check_out_hours) / Check.objects.all().count()
            avg_hours_for_employee.append(
                {'userId': employee.user.id, 'checkInAvg': check_in_avg_hours, 'checkOutAvg': check_out_avg_hours})
        response = {'values': avg_hours_for_employee}
        return Response(response)


class GetPercentOfTotalWorkingHours(APIView):
    def get(self, request):
        get_date_as_days = Check.objects.extra(select={'day': 'date( check_in )'}).values('day').annotate(
            available=Count('check_in'))
        working_hours = 0.0
        leaving_hours = 0.0
        for get_date_as_day in get_date_as_days:
            check_date_start_of_day = timezone.datetime.strptime(get_date_as_day.get('day'), '%Y-%m-%d')
            check_date_end_of_day = check_date_start_of_day + relativedelta(days=1)
            check_in_same_day = Check.objects.filter(
                Q(check_in__gte=check_date_start_of_day) & Q(check_in__lte=check_date_end_of_day))
            employees = check_in_same_day.values('employee').distinct()
            for e in employees:
                check_for_employee = check_in_same_day.filter(employee=e['employee']).order_by('-check_in').all()
                for i in range(len(check_for_employee)):
                    check = check_for_employee[i]
                    if check.check_out is not None:
                        check_in_duration = check.check_out - check.check_in
                        working_hours = working_hours + check_in_duration.total_seconds() / 3600
                    if i + 1 != len(check_for_employee):
                        next_check = check_for_employee[i + 1]
                        check_out_duration = abs(next_check.check_in - check.check_out)
                        leaving_hours = leaving_hours + check_out_duration.total_seconds() / 3600
        response = {'value': (working_hours / (working_hours + leaving_hours)) * 100}
        return Response(response)

# Create your views here.
