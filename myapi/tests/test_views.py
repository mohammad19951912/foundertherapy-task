from django.urls import reverse
from myapi.models import Check
from .test_setup import TestSetUp
from django.utils import timezone
from dateutil.relativedelta import relativedelta


class TestViews(TestSetUp):
    def test_check_in(self):
        check_in_url = reverse('check_in')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        response = self.client.post(check_in_url,
                                    {},
                                    format='json')
        self.assertEquals(response.status_code, 201)

    def test_check_out(self):
        check = Check(employee=self.employee, check_in=timezone.now())
        check.save()
        check_in_out = reverse('check_out')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        response = self.client.post(check_in_out,
                                    {},
                                    format='json')
        self.assertEquals(response.status_code, 200)

    def test_vacation(self):
        vacation = reverse('vacation')
        data = {'vacation_start_date': "2021-01-01T00:00:00Z", 'vacation_end_date': "2021-01-02T00:00:00Z"}
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        response = self.client.post(vacation,
                                    data,
                                    format='json')
        self.assertEquals(response.status_code, 201)

    def test_get_total_work(self):
        get_total_work_url = reverse('get_total_work')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        data = {'type': "year", 'year': 2021}
        response = self.client.get(get_total_work_url, data)
        self.assertEquals(response.status_code, 200)

    def test_get_average_arrive_time(self):
        check = Check(employee=self.employee, check_in=timezone.now(), check_out=timezone.now() + relativedelta(hours=1))
        check.save()
        get_average_arrive_time_url = reverse('get_average_arrive_time')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        response = self.client.get(get_average_arrive_time_url, {})
        self.assertEquals(response.status_code, 200)

    def test_get_percent_of_total(self):
        check = Check(employee=self.employee, check_in=timezone.now(), check_out=timezone.now() + relativedelta(hours=1))
        check.save()
        check1 = Check(employee=self.employee, check_in=timezone.now() + relativedelta(), check_out=timezone.now() + relativedelta(hours=1))
        check1.save()
        get_percent_of_total_url = reverse('get_percent_of_total')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user.auth_token.key)
        response = self.client.get(get_percent_of_total_url, {})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data['value'], (2/3)* 100)

