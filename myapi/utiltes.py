import time
from functools import wraps

import pandas as pd

class DjangoDataFrame(pd.DataFrame):

    def filter(
        self,
        items=None,
        like=None,
        regex=None,
        axis=None,
        type=None,
    ):
        if type is not None:
            return self.loc[self['type'] == type]
        else:
            return super(DjangoDataFrame, self).filter(
                items=items,
                like=None,
                regex=None,
                axis=None
            )


def django_dataframe(func):
    """
    Reads the django queryset as a dataframe
    :param func: function that returns Django QuerySet
    :return: DataFrame
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        result_query = func(*args, **kwargs)
        columns = [field.name for field in result_query.model._meta.fields]
        if not result_query:
            return pd.DataFrame([], columns=columns)
        time_started = time.time()
        list_values = result_query.values_list(*columns)
        result_dataframe = DjangoDataFrame(list(list_values), columns=columns)
        querying_time = time.time() - time_started
        return result_dataframe

    return wrapper
