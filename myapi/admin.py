from django.contrib import admin
from .models import Employee
from .models import Check
from .models import Vacation

admin.site.register(Employee)


@admin.register(Check)
class CheckAdmin(admin.ModelAdmin):
    list_display = ('check_in', 'check_out', 'employee')


@admin.register(Vacation)
class VacationAdmin(admin.ModelAdmin):
    list_display = ('vacation_start_date', 'vacation_end_date', 'employee')
# Register your models here.
