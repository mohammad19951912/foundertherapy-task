# myapi/urls.py
from django.urls import include, path
from rest_framework import routers
from . import views
router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('check/in', views.CheckInAPIView.as_view(), name='check_in'),
    path('check/out', views.CheckOutAPIView.as_view(), name='check_out'),
    path('vacation/', views.AddVacationAPIView.as_view(), name='vacation'),
    path('getTotalWork/', views.GetTotalWorkingHoursAPIView.as_view(), name='get_total_work'),
    path('getAverageArrivalAndLeavingTime/', views.GetAverageArrivalAndLeavingTime.as_view(), name='get_average_arrive_time'),
    path('GetPercentOfTotalWorkingHours/', views.GetPercentOfTotalWorkingHours.as_view(), name='get_percent_of_total'),
]
