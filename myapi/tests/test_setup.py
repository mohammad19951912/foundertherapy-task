from django.contrib.auth.models import User
from myapi.models import Employee
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework.authtoken.models import Token


class TestSetUp(APITestCase):

    def setUp(self):
        self.login_url = reverse('api-token-auth')
        self.login_user = {'username': "fts", 'password': "1234"}
        self.user = User.objects.create_user('samer', 'samer@thebeatles.com', 'samer')
        self.employee = Employee(user=self.user, name='samer', age=22)
        self.employee.save()
        Token.objects.create(user=self.user)
        return super().setUp()

    def tearDown(self):
        return super().tearDown()