from django.contrib.auth.models import User
from django.db import models
CHECK_CHOICES = [
    ['check_in', 'check in'],
    ['check_out', 'check out']
]


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    age = models.IntegerField()
    def __str__(self):
        return self.name


class Check(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    check_in = models.DateTimeField(auto_now_add=True)
    check_out = models.DateTimeField(null=True, blank=True)


class Vacation(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    vacation_start_date = models.DateTimeField()
    vacation_end_date = models.DateTimeField()


# Create your models here.
