# serializers.py
from datetime import timedelta
from functools import reduce

from django.utils import timezone
from rest_framework import serializers

from .models import Employee
from .models import Check
from .models import Vacation


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employee
        fields = ('name', 'age')


class CheckSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Check
        fields = ('check_in', 'check_out')
        read_only_fields = ('employee', 'check_in', 'check_out')


class VactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vacation
        fields = ('vacation_start_date', 'vacation_end_date')
        read_only_fields = ('employee',)

    def validate(self, attrs):
        employee = self.context.get('request').user.employee
        current_year = timezone.now().year
        current_year_vacations = employee.vacation_set.filter(vacation_start_date__year=current_year)

        def get_vacation_duration(total, vacation):
            vacation_duration = vacation.vacation_end_date - vacation.vacation_start_date
            return vacation_duration + total
        total_vacation_duration = reduce(get_vacation_duration, current_year_vacations, timedelta(0))
        proposed_vacation_duration = attrs.get('vacation_end_date') - attrs.get('vacation_start_date')
        total_vacation_duration = total_vacation_duration + proposed_vacation_duration
        if total_vacation_duration.days > 14:
            raise serializers.ValidationError("you exceeded max vacation for this year")
        return attrs
